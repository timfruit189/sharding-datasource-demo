package com.ttx.suite.web.advice;

import com.ttx.suite.web.excepton.BusinessException;
import com.ttx.suite.web.model.vo.Result;
import com.ttx.suite.web.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author
 * @date
 */
@ControllerAdvice
@Slf4j
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result handleException(HttpServletRequest request, HttpServletResponse response, Exception e){
        log.error(request.getRequestURI()+"出错", e);
        if(e instanceof BusinessException){
            return ResultUtil.error(e.getMessage());
        }
        if(e instanceof IllegalArgumentException){
            return ResultUtil.error(e.getMessage());
        }
        if(e instanceof IllegalStateException){
            return ResultUtil.error(e.getMessage());
        }
        return ResultUtil.error();
    }
}
