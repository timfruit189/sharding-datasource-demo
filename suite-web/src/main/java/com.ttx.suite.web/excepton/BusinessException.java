package com.ttx.suite.web.excepton;

/**
 * @author
 * @date
 */
public class BusinessException extends RuntimeException {
    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }
}
