package com.ttx.suite.web.model.vo;

import lombok.Data;

/**
 * 接口返回的基类
 *
 *
 * @author
 * @date
 */
@Data
public class Result<T> {
    /**
     * 0, 成功， -1 失败
     *
     * @see com.ttx.single.lab.model.constant.ResultCode
     */
    protected Integer code;
    protected String msg;
    protected T data;


    public Result() {
    }

    public Result(Integer code) {
        this.code = code;
    }

    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
