package com.ttx.suite.web.model.vo;

import lombok.Data;

/**
 * @author
 * @date
 */
@Data
public class PageResult<T> extends Result<T> {
    protected Long total;
    protected Long pages;
}
