package com.ttx.suite.web.model.bo;

import lombok.Data;

import java.util.List;

/**
 * @author
 * @date
 */
@Data
public class PageBO<T> {
    protected List<T> data;
    protected Long total;
    protected Long pages;

    public PageBO() {
    }

    public PageBO(List<T> data) {
        this.data = data;
    }
}
