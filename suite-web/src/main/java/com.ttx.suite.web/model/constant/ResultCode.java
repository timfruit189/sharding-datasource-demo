package com.ttx.suite.web.model.constant;

/**
 * @see com.ttx.suite.web.model.vo.Result#code
 * @author
 * @date
 */
public interface ResultCode {
    int SUCCESS_CODE =0;
    int ERROR_CODE =-1;
}
