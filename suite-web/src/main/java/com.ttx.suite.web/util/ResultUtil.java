package com.ttx.suite.web.util;


import com.ttx.suite.web.model.constant.ResultCode;
import com.ttx.suite.web.model.vo.Result;

/**
 * @author
 * @date
 */
public class ResultUtil {
    public static final int SUCCESS_CODE =ResultCode.SUCCESS_CODE,
            ERROR_CODE =ResultCode.ERROR_CODE;


    public static Result success(){
        Result result=successResult();
        return result;
    }

    public static Result success(Object data){
        Result result=successResult();
        result.setData(data);
        return result;
    }


    public static Result error(){
        Result result=new Result(ERROR_CODE, "error");
        return result;
    }

    public static Result error(String msg){
        Result result=new Result(ERROR_CODE, msg);
        return result;
    }


    private static Result successResult(){
        Result result=new Result(SUCCESS_CODE, "success");
        return result;
    }



}
