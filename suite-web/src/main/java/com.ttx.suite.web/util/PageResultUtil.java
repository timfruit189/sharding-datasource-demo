package com.ttx.suite.web.util;



import com.ttx.suite.web.model.bo.PageBO;
import com.ttx.suite.web.model.vo.PageResult;

import java.util.List;

/**
 * @author
 * @date
 */
public class PageResultUtil {
    public static final int SUCCESS_CODE = ResultUtil.SUCCESS_CODE,
            ERROR_CODE = ResultUtil.ERROR_CODE;

    public static PageResult success(List data, PageBO pageBO){
        return success(data, pageBO.getPages(), pageBO.getTotal());
    }

    public static PageResult success(List data, Long pages, Long total){
        PageResult pageResult=success();
        pageResult.setData(data);
        pageResult.setPages(pages);
        pageResult.setTotal(total);
        return pageResult;
    }



    private static PageResult success(){
        PageResult pageResult=new PageResult();
        pageResult.setCode(SUCCESS_CODE);
        pageResult.setMsg("success");
        return pageResult;
    }

    private static PageResult error(){
        PageResult pageResult=new PageResult();
        pageResult.setCode(ERROR_CODE);
        pageResult.setMsg("error");
        return pageResult;
    }

}
