package com.ttx.single.lab;

import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.single.lab.model.dto.PlaceOrderItemDto;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author TimFruit
 * @date 20-4-19 上午9:39
 */
public class OrderFactory {

    public LinkedHashMap<Integer,Long> userAddressMap =new LinkedHashMap<>();

    List<Integer> userIdList;

    public OrderFactory() {
        initUserMap();
        userIdList=userAddressMap.keySet().stream().collect(Collectors.toList());
    }

    private void initUserMap(){
        userAddressMap.put(1,1L);
        userAddressMap.put(2,2L);
        userAddressMap.put(3,3L);
        userAddressMap.put(4,4L);
        userAddressMap.put(5,5L);
        userAddressMap.put(6,6L);
        userAddressMap.put(7,7L);
        userAddressMap.put(8,8L);
    }


    public Integer randomUserId(){
        Random random=new Random();
        int index=random.nextInt(userIdList.size());
        Integer userId=userIdList.get(index);
        return userId;
    }


    public PlaceOrderDto createOrderDto(){
        PlaceOrderDto orderDto=new PlaceOrderDto();

        Integer userId=randomUserId();
        Long addressId=userAddressMap.get(userId);

        orderDto.setUserId(userId);
        orderDto.setAddressId(addressId);
        orderDto.setStatus("OK");


        List<PlaceOrderItemDto> orderItemDtos= createOrderItemDtos(userId);
        orderDto.setItemDtos(orderItemDtos);
        return orderDto;
    }

    public List<PlaceOrderItemDto> createOrderItemDtos(Integer userId){
        List<PlaceOrderItemDto> itemDtos=new ArrayList<>();

        int len=new Random().nextInt(5)+1;
        PlaceOrderItemDto itemDto=null;
        for(int i=0;i<len;i++){
            itemDto=new PlaceOrderItemDto();
            itemDto.setStatus("OK");
            itemDto.setUserId(userId);

            itemDtos.add(itemDto);
        }
        return itemDtos;
    }
}
