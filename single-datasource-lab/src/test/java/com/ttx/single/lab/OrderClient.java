package com.ttx.single.lab;

import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.suite.web.model.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TimFruit
 * @date 20-4-19 上午8:01
 */
@Slf4j
public class OrderClient {


    private String urlPrefix="http://localhost:8080/order";

    RestTemplate restTemplate=new RestTemplate();

    public void placeOrder(PlaceOrderDto placeOrderDto)  {
        final String url=urlPrefix+"/placeOrder";
        Result result=restTemplate.postForObject(url,placeOrderDto, Result.class);

        if(result.getCode()!=0){
            log.error("error: "+result.getMsg());
        }else {
            log.info("success");
        }
    }


    public void listUserOrderPage(Integer userId, Integer pageNum, Integer pageSize){
        final String url=urlPrefix+"/listUserOrderPage?userId={userId}&pageNum={pageNum}&pageSize={pageSize}";

        Result result=restTemplate.getForObject(url,Result.class, userId, pageNum, pageSize);

        if(result.getCode()!=0){
            log.error("error: "+result.getMsg());
        }else {
            log.info("success");
        }
    }

    public void selectOrder(Long orderId){
        final String url=urlPrefix+"/selectOrder?orderId={orderId}";

        Result result=restTemplate.getForObject(url,Result.class, orderId);

        if(result.getCode()!=0){
            log.error("error: "+result.getMsg());
        }else {
            log.info("success");
        }
    }


    public void cancelOrder(Long orderId,Integer userId){
        final String url=urlPrefix+"/cancelOrder?orderId={orderId}&userId={userId}";

        Result result=restTemplate.getForObject(url,Result.class, orderId, userId);

        if(result.getCode()!=0){
            log.error("error: "+result.getMsg());
        }else {
            log.info("success");
        }
    }



}
