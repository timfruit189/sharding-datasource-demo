package com.ttx.single.lab;

import cn.hutool.core.thread.NamedThreadFactory;
import com.ttx.single.lab.model.dto.PlaceOrderDto;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 模拟用户使用订单服务
 * @author TimFruit
 * @date 20-4-19 上午10:03
 */
public class MockOrderService {
    OrderClient orderClient=new OrderClient();
    OrderFactory orderFactory=new OrderFactory();

    @Test
    public void mockPlaceOrder() throws InterruptedException {

        int threadCount=2;
        ExecutorService executorService=Executors.newFixedThreadPool(threadCount,new NamedThreadFactory("placeOrderThread", false));

        int count=0;
        int total=10000;
        while(count<=total){
            count++;
            if(count%20==0){
                System.out.println("下单停顿5s");
                TimeUnit.SECONDS.sleep(5);
            }

            if(count%1000==0){
                System.out.println("下单停顿10s");
                TimeUnit.SECONDS.sleep(10);
            }

            executorService.submit(()->{
                PlaceOrderDto orderDto=orderFactory.createOrderDto();
                orderClient.placeOrder(orderDto);
            });
        }


        ThreadPoolExecutor executor=(ThreadPoolExecutor)executorService;

        long totalTaskCount=executor.getTaskCount();
        long completedTaskCount;
        while ((completedTaskCount=executor.getCompletedTaskCount())<totalTaskCount){
            System.out.println("还剩"+(totalTaskCount-completedTaskCount)+"个订单任务，等待10s");
            TimeUnit.SECONDS.sleep(10);
        }


    }




    @Test
    public void mockListUserOrderPage() throws InterruptedException {
        int count=0;
        while(true){
            count++;
            if(count%100==0){
                System.out.println("分页查询订单列表停顿5s");
                TimeUnit.SECONDS.sleep(5);
            }
            if(count%3000==0){
                //模拟低峰时期
                System.out.println("分页查询订单列表停顿300s");
                TimeUnit.SECONDS.sleep(300);
            }
            Integer userId=orderFactory.randomUserId();
            int pageNum=new Random().nextInt(10)+1;
            int pageSize=new Random().nextInt(20)+1;
            orderClient.listUserOrderPage(userId, pageNum,pageSize);
        }
    }



    @Test
    public void mockSelectOrder() throws InterruptedException {
        int count=0;
        while(true){
            count++;
            if(count%100==0){
                System.out.println("查询订单停顿5s");
                TimeUnit.SECONDS.sleep(5);
            }
            int orderId=new Random().nextInt(30000);
            orderClient.selectOrder((long)orderId);
        }
    }



    @Test
    public void mockCancelOrder() throws InterruptedException {
        int count=0;
        while(true){
            count++;
            if(count%5==0){
                System.out.println("取消订单停顿5s");
                TimeUnit.SECONDS.sleep(5);
            }
            if(count%1000==0){
                //模拟低峰时期
                System.out.println("分页查询订单列表停顿300s");
                TimeUnit.SECONDS.sleep(300);
            }
            int orderId=new Random().nextInt(30000);
            int userId=orderFactory.randomUserId();
            orderClient.cancelOrder((long)orderId, userId);
        }
    }


}
