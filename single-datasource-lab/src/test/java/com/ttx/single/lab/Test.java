package com.ttx.single.lab;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author TimFruit
 * @date 20-4-24 上午8:47
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class Test {

    @Autowired
    @Qualifier("shardingDataSource")
    DataSource shardingDataSource;


//    @org.junit.Test
    public void test() throws SQLException {
        Connection conn=shardingDataSource.getConnection();
        Statement stmt=conn.createStatement();
//        stmt.executeQuery("select * from t_order where user_id=1");
        stmt.execute("insert into t_order(order_id, user_id, address_id, status, create_time, update_time) " +
                "values(1,8,8,'OK', '2019-03-01 00:00:00', '2019-03-01 00:00:00')");

    }

}
