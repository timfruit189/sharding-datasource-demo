package com.ttx.single.lab;

import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.single.lab.model.dto.PlaceOrderItemDto;
import com.ttx.single.lab.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

/**
 * @author TimFruit
 * @date 20-4-18 下午6:52
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class InitData  {

    @Autowired
    OrderService defaultOrderService;






    //初始化单库时订单的数据
//    @Test
    public void initOrder(){

        int total=30000;

        OrderFactory orderFactory=new OrderFactory();

        PlaceOrderDto orderDto=null;
        for(int i=0;i<total;i++){

            if(i%100==0){
                System.out.println("i: "+ i);
            }

            orderDto=orderFactory.createOrderDto();

            defaultOrderService.placeOrder(orderDto);
        }



    }





}
