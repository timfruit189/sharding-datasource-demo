package com.ttx.single.lab.config;

import com.ttx.single.lab.mapper.OrderItemMapper;
import com.ttx.single.lab.mapper.OrderMapper;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * 单库时的配置
 * @author TimFruit
 * @date 20-2-7 下午8:00
 */
@Configuration
@MapperScan(basePackages = {"com.ttx.single.lab.mapper"})
public class MybatisConfig {
    @Autowired
    DefaultDatasourceProperties datasourceProperties;
    @Value("${mybatis.mapper-locations}")
    String mapperLocations;



    @Bean("defaultDataSource")
    @Primary
    public DataSource createDataSource(){
        HikariDataSource dataSource=new HikariDataSource();
        dataSource.setDriverClassName(datasourceProperties.getDriverClassName());
        dataSource.setJdbcUrl(datasourceProperties.getUrl());
        dataSource.setUsername(datasourceProperties.getUsername());
        dataSource.setPassword(datasourceProperties.getPassword());
        return dataSource;
    }


    @Bean("defaultSqlSessionFactory")
    @Primary
    public SqlSessionFactory createSqlSessionFactory(@Qualifier("defaultDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean=new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        PathMatchingResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
        factoryBean.setMapperLocations(resolver.getResources(mapperLocations));
        SqlSessionFactory sqlSessionFactory = factoryBean.getObject();
        return sqlSessionFactory;
    }


    @Bean("defaultTransactionManager")
    @Primary
    public TransactionManager createTransactionManager(@Qualifier("defaultDataSource") DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }



    // =====================

    //还有一个名为orderMapper的bean,  数据源也是单库
    @Bean("singleOrderMapper")
    public OrderMapper createOrderMapper(@Qualifier("defaultSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderMapper.class);
        return factoryBean.getObject();
    }


    @Bean("singleOrderItemMapper")
    public OrderItemMapper createOrderItemMapper(@Qualifier("defaultSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderItemMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderItemMapper.class);
        return factoryBean.getObject();
    }

}
