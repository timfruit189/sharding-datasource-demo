package com.ttx.single.lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author TimFruit
 * @date 20-2-7 下午9:11
 */
//配置基本扫描包为com.ttx
@SpringBootApplication(scanBasePackages = {"com.ttx"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
