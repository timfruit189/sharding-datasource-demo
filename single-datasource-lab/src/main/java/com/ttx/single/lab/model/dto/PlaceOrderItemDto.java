package com.ttx.single.lab.model.dto;

import lombok.Data;

/**
 * @author TimFruit
 * @date 20-4-8 上午8:18
 */
@Data
public class PlaceOrderItemDto {
//    private long orderItemId;

//    private long orderId;

    private int userId;

    private String status;
}
