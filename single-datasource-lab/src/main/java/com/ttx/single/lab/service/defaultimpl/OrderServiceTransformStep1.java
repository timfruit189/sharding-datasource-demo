package com.ttx.single.lab.service.defaultimpl;

import com.ttx.single.lab.mapper.AddressMapper;
import com.ttx.single.lab.mapper.OrderMapper;
import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.single.lab.service.CommonDoubleWriteOrderService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 1
 * # 1 使用uid的初始下单  2 下单双写，以单库读写为主  3  下单双写， 以分库读写为主 4 下单只写在分库
 * 使用了uid生成订单id
 * @author TimFruit
 * @date 20-4-11 下午2:48
 */
@Service
public class OrderServiceTransformStep1 extends OrderServiceImpl implements InitializingBean {
    @Autowired
    CommonDoubleWriteOrderService doubleWriteOrderService;
    @Autowired
    ApplicationContext applicationContext;

    @Transactional
    @Override
    public void placeOrder(PlaceOrderDto orderDto) {
        doubleWriteOrderService.placeOrder(orderMapper,orderItemMapper,orderDto);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("=======================xxxxxxxxxxxxxxxxxxx");
        OrderMapper orderMapper=applicationContext.getBean("singleOrderMapper", OrderMapper.class);
        System.out.println(orderMapper);
        Map map=applicationContext.getBeansOfType(OrderMapper.class);
        System.out.println(map);

        map=applicationContext.getBeansOfType(AddressMapper.class);
        System.out.println(map);
        System.out.println("=======================xxxxxxxxxxxxxxxxxxx");
    }
}
