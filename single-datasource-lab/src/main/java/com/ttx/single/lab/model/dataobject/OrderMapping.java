package com.ttx.single.lab.model.dataobject;

import lombok.Data;

import java.util.Date;

/**
 * 辅助orderId找到userId的对象， 减少分库分表后的查询 (全局搜索)
 *
 * @author TimFruit
 * @date 20-4-11 上午10:07
 */
@Data
public class OrderMapping {
    private Long id;
    private Long orderId;
    private Integer userId;
    private Date createTime;
    private Date updateTime;
}
