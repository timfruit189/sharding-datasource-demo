package com.ttx.single.lab.mapper;

import com.ttx.single.lab.model.dataobject.OrderMapping;
import org.apache.ibatis.annotations.Param;

/**
 * @author TimFruit
 * @date 20-4-11 上午10:06
 */
public interface OrderMappingMapper {
    /**
     * insert data.
     *
     * @param entity entity
     * @return generated primary key
     */
    Integer insert(OrderMapping entity);


    Integer selectUserIdByOrderId(@Param("orderId") Long orderId);

    void replace(OrderMapping mapping);
}
