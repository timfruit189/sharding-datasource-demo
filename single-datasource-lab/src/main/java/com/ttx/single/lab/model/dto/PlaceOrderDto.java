package com.ttx.single.lab.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @author TimFruit
 * @date 20-4-8 上午8:15
 */
@Data
public class PlaceOrderDto {

//    private long orderId;

    private int userId;

    private long addressId;

    private String status;


    private List<PlaceOrderItemDto> itemDtos;
}
