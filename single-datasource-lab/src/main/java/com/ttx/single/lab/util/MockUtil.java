package com.ttx.single.lab.util;

import java.util.Random;

/**
 * @author TimFruit
 * @date 20-4-13 上午11:22
 */
public class MockUtil {
    public static void randomException(){
        Random random=new Random();
        int ran=random.nextInt(10);
        if(ran==0){
            throw new RuntimeException("模拟随机异常");
        }
    }
}
