package com.ttx.single.lab.service.transform;

import com.baidu.fsg.uid.UidGenerator;
import com.ttx.single.lab.mapper.OrderItemMapper;
import com.ttx.single.lab.mapper.OrderMapper;
import com.ttx.single.lab.model.bo.OrderBO;
import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.single.lab.service.CommonDoubleWriteOrderService;
import com.ttx.single.lab.service.OrderService;
import com.ttx.single.lab.service.defaultimpl.OrderServiceTransformStep1;
import com.ttx.single.lab.service.sharding.OrderServiceShardingImpl;
import com.ttx.suite.web.model.bo.PageBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author TimFruit
 * @date 20-4-8 下午6:44
 */
@Service
@Slf4j
public class OrderServiceTransformImpl implements OrderService,InitializingBean {
    //单库
    @Autowired
    @Qualifier("singleOrderMapper")
    OrderMapper singleOrderMapper;
    @Autowired
    @Qualifier("singleOrderItemMapper")
    OrderItemMapper singleOrderItemMapper;


    //分库分表
    @Autowired
    @Qualifier("shardingOrderMapper")
    OrderMapper shardingOrderMapper;
    @Autowired
    @Qualifier("shardingOrderItemMapper")
    OrderItemMapper shardingOrderItemMapper;

    @Autowired
    UidGenerator uidGenerator;


    @Value("${transfrom.step}")
    int transformStep;

    //单库下单操作， 改了下单的方法，使用uid
    @Autowired
    OrderServiceTransformStep1 orderServiceTransformStep1;

    @Autowired
    OrderServiceTransformStep2 orderServiceTransformStep2;
    @Autowired
    OrderServiceTransformStep3 orderServiceTransformStep3;

    //分库下单操作
    @Autowired
    OrderServiceShardingImpl orderServiceSharding;

    @Autowired
    CommonDoubleWriteOrderService doubleWriteOrderService;


    Map<Integer, OrderService> stepServiceMap=new ConcurrentHashMap<>();


    @Override
    public void afterPropertiesSet() throws Exception {
        stepServiceMap.put(1, orderServiceTransformStep1);
        stepServiceMap.put(2, orderServiceTransformStep2);
        stepServiceMap.put(3, orderServiceTransformStep3);
        stepServiceMap.put(4, orderServiceSharding);

        log.info("配置[transfrom.step]={}",transformStep);
        Assert.state(transformStep>=1 && transformStep<=4, "配置[transfrom.step]需在[1,4]范围内");
    }



    @Override
    public void placeOrder(PlaceOrderDto orderDto) {
        stepServiceMap.get(transformStep).placeOrder(orderDto);
    }




    @Override
    public PageBO<OrderBO> listUserOrderPage(Long userId, Integer pageNum, Integer pageSize) {

        return stepServiceMap.get(transformStep).listUserOrderPage(userId, pageNum, pageSize);
    }

    @Override
    public OrderBO selectOrder(Long orderId) {
        return stepServiceMap.get(transformStep).selectOrder(orderId);
    }

    @Override
    public void cancelOrder(Long orderId, Integer userId) {
        stepServiceMap.get(transformStep).cancelOrder(orderId, userId);
    }


}
