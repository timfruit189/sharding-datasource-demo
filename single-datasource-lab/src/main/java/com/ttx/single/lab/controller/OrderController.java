package com.ttx.single.lab.controller;

import com.ttx.single.lab.model.bo.OrderBO;
import com.ttx.single.lab.model.bo.OrderItemBO;
import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.single.lab.model.vo.OrderItemVO;
import com.ttx.single.lab.model.vo.OrderVO;
import com.ttx.single.lab.service.defaultimpl.OrderServiceImpl;
import com.ttx.single.lab.service.sharding.OrderServiceShardingImpl;
import com.ttx.single.lab.service.transform.OrderServiceTransformImpl;
import com.ttx.suite.web.model.bo.PageBO;
import com.ttx.suite.web.model.vo.PageResult;
import com.ttx.suite.web.model.vo.Result;
import com.ttx.suite.web.util.ConvertUtil;
import com.ttx.suite.web.util.PageResultUtil;
import com.ttx.suite.web.util.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author TimFruit
 * @date 20-4-12 上午10:28
 */
@Api("订单模块")
@RestController
@RequestMapping("/order")
public class OrderController {

    //刚开始时使用的实例
    @Autowired
    @Qualifier("defaultOrderService")
    OrderServiceImpl orderService;

    //分库分表迁移时使用的实例
    @Autowired
    OrderServiceTransformImpl orderServiceTransform;

    //完成分库分表迁移后使用的实例
    @Autowired
    OrderServiceShardingImpl orderServiceSharding;



    @PostMapping("/placeOrder")
    @ApiOperation(value = "下单")
    @ApiImplicitParams({
    })
    public Result placeOrder(@RequestBody PlaceOrderDto orderDto){
        orderServiceTransform.placeOrder(orderDto);
        return ResultUtil.success();
    }


    @GetMapping("/listUserOrderPage")
    @ApiOperation(value = "分页查询用户的订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "userId", dataType = "int", example = "1" ),
            @ApiImplicitParam(name = "pageNum", value = "页下标, >=1", dataType = "int", example = "1" ),
            @ApiImplicitParam(name = "pageSize", value = "页大小, >=1", dataType = "long", example = "1"),
    })
    public PageResult<List<OrderBO>> listUserOrderPage(Long userId, Integer pageNum, Integer pageSize) {
        PageBO<OrderBO> pageBO=orderServiceTransform.listUserOrderPage(userId,pageNum, pageSize);

        List<OrderBO> orderBOs=pageBO.getData();
        return PageResultUtil.success(orderBOs, pageBO);
    }



    @GetMapping("/selectOrder")
    @ApiOperation(value = "查询订单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "orderId", dataType = "int", example = "1" ),
    })
    public Result<OrderBO> selectOrder(@RequestParam("orderId") Long orderId){
        OrderBO orderBO=orderServiceTransform.selectOrder(orderId);
        return ResultUtil.success(orderBO);
    }


    @GetMapping("/cancelOrder")
    @ApiOperation(value = "取消订单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId", value = "orderId", dataType = "int", example = "1" ),
    })
    public Result cancelOrder(@RequestParam("orderId") Long orderId,@RequestParam("userId") Integer userId){
        orderServiceTransform.cancelOrder(orderId, userId);
        return ResultUtil.success();
    }


}
