package com.ttx.single.lab.service;

import com.ttx.single.lab.model.bo.OrderBO;
import com.ttx.single.lab.model.dto.PlaceOrderDto;
import com.ttx.suite.web.model.bo.PageBO;

import java.util.List;

/**
 * @author TimFruit
 * @date 20-4-8 上午8:09
 */
public interface OrderService {
    /**
     * 下单
     */
    void placeOrder(PlaceOrderDto orderDto);


    /**
     * 分页查询
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageBO<OrderBO> listUserOrderPage(Long userId, Integer pageNum, Integer pageSize);



    // 根据订单id查询订单
    //warning 用userId切分， 这个怎么查？
    // 1. 冗余关联表 orderId-userId， 多查一次,加缓存优化， 性能上多一层网络开销  (选择这种)
    // 2. 冗余全量表 即用orderId再完整切分一次， 成本较大

    OrderBO selectOrder(Long orderId);

    void cancelOrder(Long orderId, Integer userId);
}
