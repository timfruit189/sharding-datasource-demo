/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ttx.single.lab.mapper;


import com.ttx.single.lab.model.dataobject.OrderItemDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderItemMapper extends CommonMapper<OrderItemDO, Long> {

    @Deprecated
    Long insert(OrderItemDO entity);

    /**
     * 自定义id
     * @param entity
     * @return
     */
    Long insertWidthId(OrderItemDO entity);

    List<OrderItemDO> listUserOrderItem(
            @Param("userId") Long userId, @Param("orderIds") List<Long> orderIds);

    List<OrderItemDO> listOrderItemByOrderId(@Param("orderId") Long orderId);

    List<OrderItemDO> listOrderItemByOrderIdAndUserId(@Param("orderId")Long orderId, @Param("userId")Integer userId);
}
