/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ttx.single.lab.mapper;


import com.ttx.single.lab.model.dataobject.OrderDO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderMapper extends CommonMapper<OrderDO, Long> {
    @Deprecated
    Long insert(OrderDO entity);

    /**
     * 自定义id
     * @param entity
     * @return
     */
    Long insertWidthId(OrderDO entity);

    List<OrderDO> listUserOrderPage(
            @Param("userId") Long userId,
            @Param("pageIndex") Integer pageIndex,
            @Param("pageSize") Integer pageSize);

    Long countUserOrder(@Param("userId") Long userId);

    OrderDO selectByOrderId(@Param("orderId") Long orderId);

    OrderDO selectByOrderIdAndUserId(@Param("orderId") Long orderId, @Param("userId") Integer userId);

    void updateOrderStatus(@Param("orderId")Long orderId,
                           @Param("userId") Integer userId,
                           @Param("status") String status,
                           @Param("updateTime")Date updateTime);
}
