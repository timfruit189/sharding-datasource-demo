package com.ttx.single.lab.model.bo;

import lombok.Data;

import java.util.List;

/**
 * @author TimFruit
 * @date 20-4-10 上午10:50
 */
@Data
public class OrderBO {
    private long orderId;

    private int userId;

    private long addressId;

    private String status;


    private List<OrderItemBO> orderItemBOList;


    @Override
    public String toString() {
        return String.format("order_id: %s, user_id: %s, address_id: %s, status: %s", orderId, userId, addressId, status);
    }
}
