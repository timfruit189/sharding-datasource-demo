package com.ttx.single.lab.model.vo;

import lombok.Data;

/**
 * @author TimFruit
 * @date 20-4-12 上午10:46
 */
@Data
public class OrderItemVO {
    private long orderItemId;

    private long orderId;

    private int userId;

    private String status;
}
