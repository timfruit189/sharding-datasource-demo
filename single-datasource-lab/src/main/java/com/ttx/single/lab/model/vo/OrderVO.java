package com.ttx.single.lab.model.vo;

import com.ttx.single.lab.model.bo.OrderItemBO;
import lombok.Data;

import java.util.List;

/**
 * @author TimFruit
 * @date 20-4-12 上午10:45
 */
@Data
public class OrderVO {
    private long orderId;

    private int userId;

    private long addressId;

    private String status;


    private List<OrderItemVO> orderItemVOList;
}
