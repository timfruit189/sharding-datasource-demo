package com.ttx.single.lab.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author TimFruit
 * @date 20-4-11 下午5:32
 */
@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
@Data
public class DefaultDatasourceProperties {
    private String driverClassName;

    private String url;

    private String username;

    private String password;
}
