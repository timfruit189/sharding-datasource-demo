package com.ttx.single.lab.config;

import com.ttx.single.lab.mapper.OrderItemMapper;
import com.ttx.single.lab.mapper.OrderMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration;
import org.apache.shardingsphere.shardingjdbc.spring.boot.sharding.SpringBootShardingRuleConfigurationProperties;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;

import javax.sql.DataSource;

/**
 * sharding 配置
 * @author TimFruit
 * @date 20-4-11 下午5:01
 */
@Configuration
public class MybatisShardingConfig {

    /**
     * @see SpringBootConfiguration#shardingDataSource()
     */
    @Autowired
    @Qualifier("shardingDataSource")
    DataSource shardingDataSource;

    @Value("${mybatis.mapper-locations}")
    String mapperLocations;



    @Bean("shardingSqlSessionFactory")
    public SqlSessionFactory createSqlSessionFactory(@Qualifier("shardingDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean=new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        PathMatchingResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
        factoryBean.setMapperLocations(resolver.getResources(mapperLocations));
        SqlSessionFactory sqlSessionFactory = factoryBean.getObject();
        return sqlSessionFactory;
    }




    @Bean("shardingTransactionManager")
    public TransactionManager createTransactionManager(@Qualifier("shardingDataSource") DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean("shardingOrderMapper")
    public OrderMapper createOrderMapper(@Qualifier("shardingSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderMapper.class);
        return factoryBean.getObject();
    }


    @Bean("shardingOrderItemMapper")
    public OrderItemMapper createOrderItemMapper(@Qualifier("shardingSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderItemMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderItemMapper.class);
        return factoryBean.getObject();
    }





}
