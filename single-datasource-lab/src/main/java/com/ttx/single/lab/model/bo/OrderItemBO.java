package com.ttx.single.lab.model.bo;

import lombok.Data;

/**
 * @author TimFruit
 * @date 20-4-10 上午10:50
 */
@Data
public class OrderItemBO {
    private long orderItemId;

    private long orderId;

    private int userId;

    private String status;


    @Override
    public String toString() {
        return String.format("order_item_id:%s, order_id: %s, user_id: %s, status: %s", orderItemId, orderId, userId, status);
    }
}
