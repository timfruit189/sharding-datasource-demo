

sharding-proxy代理使用的配置
这里使用的配置是原生yml配置

和springboot的配置有些不一样
注意事项: 
1. proxy的配置规定后缀为.yaml,  config-前缀   
2. proxy的配置属性必须为驼峰式 'database-strategy'错误,'databaseStrategy'正确
3. 数据源别名'ds-orders-0' 这样比较好，而不是'dsOrders0', 不然springboot2会报错， 这样两种都可以兼容


配置见 https://shardingsphere.apache.org/document/current/en/manual/sharding-proxy/configuration/

    
连接如下所示  
```
jdbc:mysql://127.0.0.1:3308/sharding_db?useSSL=false&useUnicode=true&characterEncoding=UTF-8
```
账号密码均为 sharding     












