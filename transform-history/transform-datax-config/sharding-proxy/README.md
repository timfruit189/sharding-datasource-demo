## 使用datax + sharding-proxy 迁移



一、 由于代理不兼容datax的批量写入方式，采用一条条插入的方式，速度很慢



二、使用前需要替换plugin-rdbms-util-0.0.1-SNAPSHOT.jar包

使用datax时，datax默认会将long,int等类型使用string类型替换，
如果是原生数据库，则会进行转换，但是使用sharing-proxy则不兼容，
抛出以下异常
```
java.sql.SQLException: 2Unknown exception: [No signature of method: java.lang.String.mod() is applicable for argument types: (java.lang.Integer) values: [2]
Possible solutions: drop(int), find(), any(), find(groovy.lang.Closure), take(int), any(groovy.lang.Closure)]
	at com.mysql.jdbc.SQLError.createSQLException(SQLError.java:996) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.MysqlIO.checkErrorPacket(MysqlIO.java:3887) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.MysqlIO.checkErrorPacket(MysqlIO.java:3823) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.MysqlIO.sendCommand(MysqlIO.java:2435) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.MysqlIO.sqlQueryDirect(MysqlIO.java:2582) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.ConnectionImpl.execSQL(ConnectionImpl.java:2530) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.PreparedStatement.executeInternal(PreparedStatement.java:1907) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.mysql.jdbc.PreparedStatement.execute(PreparedStatement.java:1199) ~[mysql-connector-java-5.1.34.jar:5.1.34]
	at com.alibaba.datax.plugin.rdbms.writer.CommonRdbmsWriter$Task.doOneInsert(CommonRdbmsWriter.java:382) [plugin-rdbms-util-0.0.1-SNAPSHOT.jar:na]
	at com.alibaba.datax.plugin.rdbms.writer.CommonRdbmsWriter$Task.doBatchInsert(CommonRdbmsWriter.java:362) [plugin-rdbms-util-0.0.1-SNAPSHOT.jar:na]
	at com.alibaba.datax.plugin.rdbms.writer.CommonRdbmsWriter$Task.startWriteWithConnection(CommonRdbmsWriter.java:291) [plugin-rdbms-util-0.0.1-SNAPSHOT.jar:na]
	at com.alibaba.datax.plugin.rdbms.writer.CommonRdbmsWriter$Task.startWrite(CommonRdbmsWriter.java:319) [plugin-rdbms-util-0.0.1-SNAPSHOT.jar:na]
	at com.alibaba.datax.plugin.writer.mysqlwriter.MysqlWriter$Task.startWrite(MysqlWriter.java:78) [mysqlwriter-0.0.1-SNAPSHOT.jar:na]
	at com.alibaba.datax.core.taskgroup.runner.WriterRunner.run(WriterRunner.java:56) [datax-core-0.0.1-SNAPSHOT.jar:na]
	at java.lang.Thread.run(Thread.java:748) [na:1.8.0_171]
```


本人以对plugin-rdbms-util模块下的com.alibaba.datax.plugin.rdbms.writer.CommonRdbmsWriter
进行修改
```
    //----------- 修改了这里 2020-04-20
                // java.sql.SQLException: 2Unknown exception: [No signature of method: java.lang.String.mod() is applicable for argument types: (java.lang.Integer) values: [2]
                // 如果不改，datax默认使用String类型， sharding-proxy会报错
                case Types.INTEGER:
                    Long iv=column.asLong();
                    if(iv==null){
                        preparedStatement.setString(columnIndex + 1, null);
                    }else {
                        preparedStatement.setInt(columnIndex + 1, iv.intValue());
                    }
                    break;
                case Types.BIGINT:
                    Long lv=column.asLong();
                    if(lv==null){
                        preparedStatement.setString(columnIndex + 1, null);
                    }else {
                        preparedStatement.setLong(columnIndex + 1, lv);
                    }
                    break;
                case Types.DECIMAL:
                    BigDecimal bgv=column.asBigDecimal();
                    if(bgv==null){
                        preparedStatement.setString(columnIndex + 1, null);
                    }else {
                        preparedStatement.setBigDecimal(columnIndex + 1, bgv);
                    }
                    break;
                case Types.DOUBLE:
                case Types.FLOAT:
                    Double dv=column.asDouble();
                    if(dv==null){
                        preparedStatement.setString(columnIndex + 1, null);
                    }else {
                        preparedStatement.setDouble(columnIndex + 1, dv);
                    }
                    break;
                 // ----------
                case Types.SMALLINT:
                case Types.NUMERIC:
                case Types.REAL:
                    String strValue = column.asString();
                    if (emptyAsNull && "".equals(strValue)) {
                        preparedStatement.setString(columnIndex + 1, null);
                    } else {
                        preparedStatement.setString(columnIndex + 1, strValue);
                    }
                    break;
```


打包编译后的jar包为plugin-rdbms-util-0.0.1-SNAPSHOT.jar

替换plugin/writer/mysqlwriter/libs/plugin-rdbms-util-0.0.1-SNAPSHOT.jar即可


注意，如果bigint等类型为空，可能还会出错，需要设置默认值



