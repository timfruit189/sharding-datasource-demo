## 使用datax + sharding-jdbc 迁移(推荐)

 datax和sharding-jdbc整合 项目地址 https://github.com/TimFruit/DataX
 
 
 优点： 较为契合的整合了datax，同步速度很快
 
 
 ### 整合sharding-jdbc的datax使用方法
 
 1.将sharding-plugin-rdbms-util-20200423-1428.zip libs下的jar包替换writer或reader libs下的jar包
 
 2.在job中的writer或reader配置 添加 
 ```
 "shardingType":"shardingJdbc",
 "shardingConfigPath":"job/sharding",
 ```
 
 3.在job/sharding文件夹下添加sharding在springboot中使用的yml配置
 
 
 更具体使用方法查看[项目文档](https://github.com/TimFruit/DataX/tree/master/plugin-rdbms-util)
 
 
 
 

