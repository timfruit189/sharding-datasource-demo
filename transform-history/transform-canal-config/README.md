使用canal迁移


canal和sharding-jdbc整合项目地址rdb-sharidng  https://github.com/TimFruit/canal/tree/master/client-adapter 


canal比较适合增量同步


由于mysql的机制，binlog日志可能不完整，如只会保证10天内的日志
线上运行比较久的项目就不是很适合了


参考mysql配置主从同步(https://zixuephp.net/article-438.html)，
1. 使用`show master status`查询当前日志位置，截图保存，备用
2. 使用mysql dump命令导出需要具体的某几张表历史数据
3. 在测试环境配置临时mysql， 启动binlog
4. 配置canal为临时mysql的从节点，
5. 配置canal-adapter, 同步到分库分表
6. 导入表历史数据，重放binlog，进行同步


