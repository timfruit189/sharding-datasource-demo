package com.ttx.uid.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.ExecutorType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

/**
 * 参照 test/resources/mybatis-spring.xml
 * @author TimFruit
 * @date 20-4-8 上午9:37
 */
@Slf4j
@Configuration
public class UidMybatisConfig  {


    @Autowired
    UidDataSourceProperties uidDataSourceProperties;



    @Bean("uidSqlSessionFactoryBean")
    public SqlSessionFactoryBean createUidSqlSessionFactoryBean() throws IOException {
        DataSource dataSource=createDataSource(uidDataSourceProperties);

        SqlSessionFactoryBean factoryBean=new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        PathMatchingResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
        Resource[] resources=resolver.getResources("classpath:META-INF/mybatis/mapper/WORKER*.xml");
        factoryBean.setMapperLocations(resources);


        return factoryBean;
    }




    // ---------------------


    // warning 不配置事务管理器
    // uid生成器唯一使用事务的地方在DisposableWorkerIdAssigner#assignWorkerId
    // spring的事务管理是基于datasource，由于有多个datasource， 本uid的datasource不对应默认的dataource，
    // 所以DisposableWorkerIdAssigner#assignWorkerId事务不生效
    // 该方法内只使用了一个插入数据方法，即时该方法内的事务不生效也没问题
    // org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration


    //    @Bean("uidDataSource") 去掉，无需受 spring管理， 是为了不影响默认的datasource
    public DataSource createDataSource(UidDataSourceProperties sourceProperties){
        HikariDataSource dataSource=new HikariDataSource();
        dataSource.setJdbcUrl(sourceProperties.getUrl());
        dataSource.setDriverClassName(sourceProperties.getDriverClassName());
        dataSource.setUsername(sourceProperties.getUsername());
        dataSource.setPassword(sourceProperties.getPassword());
        return dataSource;
    }




}