package com.ttx.uid.config;

import lombok.Data;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.jdbc.DataSourceInitializationMode;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author TimFruit
 * @date 20-4-8 上午9:44
 */
@Component
@Data
public class UidDataSourceProperties  {

    @Value("${spring.uid.datasource.driver-class-name}")
    private String driverClassName;

    @Value("${spring.uid.datasource.url}")
    private String url;

    @Value("${spring.uid.datasource.username}")
    private String username;

    @Value("${spring.uid.datasource.password}")
    private String password;
}
