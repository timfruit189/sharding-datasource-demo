package com.ttx.uid.config;

import org.apache.ibatis.session.ExecutorType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

/**
 * @author TimFruit
 * @date 20-4-8 下午2:52
 */
@Configuration
public class UidMybatisScannerConfig {
    @Bean("uidMapperScannerConfigurer")
    public static MapperScannerConfigurer createUidMapperScannerConfigurer() throws Exception {
        MapperScannerConfigurer configurer=new MapperScannerConfigurer();

        configurer.setSqlSessionFactoryBeanName("uidSqlSessionFactoryBean");

        configurer.setAnnotationClass(Repository.class);
        configurer.setBasePackage("com.baidu.fsg.uid.worker.dao");

        return configurer;
    }

    //无须配置bean
    public SqlSessionTemplate createUidSqlSessionTemplate(SqlSessionFactoryBean sqlSessionFactoryBean) throws Exception {
        SqlSessionTemplate batchTemplate=new SqlSessionTemplate(sqlSessionFactoryBean.getObject(), ExecutorType.BATCH);
        return batchTemplate;
    }
}
