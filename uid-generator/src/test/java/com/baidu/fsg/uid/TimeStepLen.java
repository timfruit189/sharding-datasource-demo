package com.baidu.fsg.uid;

import org.junit.Test;

/**
 * @author TimFruit
 * @date 20-4-21 下午1:57
 */
public class TimeStepLen {
    //计算uid随着时间的变化，uid的间隔范围
    @Test
    public void calcStepLen(){
//        uidGenerator.setEpochStr("2020-01-01"); //起始时间
//        uidGenerator.setTimeBits(31); // 约可支持69.6年, 一旦投入生产，时间将不可更改，不然会重复， 利用时间的递增性，达到单个机器id的不重复
//        uidGenerator.setWorkerBits(17); // 机器id  13w次 由于使用了markWorkerId，可以调小一点   以下两个参数可以改
//        uidGenerator.setSeqBits(15);

        long stepLen=1;
        stepLen=stepLen<<17;
        stepLen=stepLen<<15;
        //1秒
        //4294967296
        System.out.println(stepLen);
    }
}
