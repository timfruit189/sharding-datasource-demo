package com.baidu.fsg.uid;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.baidu.fsg.uid.worker.DisposableWorkerIdAssigner;
import com.baidu.fsg.uid.worker.MarkWorkerIdAssignerWrapper;
import com.baidu.fsg.uid.worker.WorkerIdAssigner;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author TimFruit
 * @date 20-4-12 下午6:45
 */
@Configuration
public class MarkWorkerIdAssignerWrapperConfig {


    @Bean("markWorkerIdAssigner2")
    public WorkerIdAssigner createMarkWorkerIdAssigner(
            @Qualifier("disposableWorkerIdAssigner") WorkerIdAssigner assigner){
        MarkWorkerIdAssignerWrapper wrapper=new MarkWorkerIdAssignerWrapper(assigner);
        return wrapper;
    }

    @Bean("uidGenerator2")
    public CachedUidGenerator uidGenerator2(@Qualifier("markWorkerIdAssigner2") WorkerIdAssigner assigner){
        CachedUidGenerator uidGenerator=new CachedUidGenerator();
        uidGenerator.setWorkerIdAssigner(assigner);
        uidGenerator.setEpochStr("2020-01-01"); //起始时间
        uidGenerator.setTimeBits(31); // 约可支持69.6年, 一旦投入生产，时间将不可更改，不然会重复， 利用时间的递增性，达到单个机器id的不重复
        uidGenerator.setWorkerBits(17); // 机器id  13w次 由于使用了markWorkerId，可以调小一点   以下两个参数可以改
        uidGenerator.setSeqBits(15); //  可支持每秒32768 个并发

        return uidGenerator;
    }




    //-------------------

    @Bean("disposableWorkerIdAssigner2")
    public DisposableWorkerIdAssigner createDisposableWorkerIdAssigner(){
        DisposableWorkerIdAssigner assigner=new DisposableWorkerIdAssigner();
        return assigner;
    }

    @Bean("markWorkerIdAssigner3")
    public WorkerIdAssigner createMarkWorkerIdAssigner3(
            @Qualifier("disposableWorkerIdAssigner2") WorkerIdAssigner assigner){
        MarkWorkerIdAssignerWrapper wrapper=new MarkWorkerIdAssignerWrapper(assigner);
        return wrapper;
    }



    @Bean("uidGenerator3")
    public CachedUidGenerator uidGenerator3(@Qualifier("markWorkerIdAssigner3") WorkerIdAssigner assigner){
        CachedUidGenerator uidGenerator=new CachedUidGenerator();
        uidGenerator.setWorkerIdAssigner(assigner);
        uidGenerator.setEpochStr("2020-01-01"); //起始时间
        uidGenerator.setTimeBits(31); // 约可支持69.6年, 一旦投入生产，时间将不可更改，不然会重复， 利用时间的递增性，达到单个机器id的不重复
        uidGenerator.setWorkerBits(17); // 机器id  13w次 由于使用了markWorkerId，可以调小一点   以下两个参数可以改
        uidGenerator.setSeqBits(15); //  可支持每秒32768 个并发

        return uidGenerator;
    }
}
