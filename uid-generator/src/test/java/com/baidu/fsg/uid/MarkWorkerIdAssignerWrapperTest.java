package com.baidu.fsg.uid;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.baidu.fsg.uid.worker.MarkWorkerIdAssignerWrapper;
import com.baidu.fsg.uid.worker.WorkerIdAssigner;
import com.ttx.uid.config.UidDataSourceProperties;
import com.ttx.uid.config.UidDefaultGeneratorConfig;
import com.ttx.uid.config.UidMybatisConfig;
import com.ttx.uid.config.UidMybatisScannerConfig;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author TimFruit
 * @date 20-4-12 下午6:06
 */
@SpringBootTest
@ActiveProfiles("uid")
@ComponentScan("com.baidu")
@ContextConfiguration(classes = {UidDataSourceProperties.class,
        UidMybatisConfig.class,UidMybatisScannerConfig.class,
        UidDefaultGeneratorConfig.class,MarkWorkerIdAssignerWrapperConfig.class})
public class MarkWorkerIdAssignerWrapperTest  {

    @Autowired
    @Qualifier("cachedUidGenerator")
    CachedUidGenerator cachedUidGenerator;




    //测试注入不同markAssigner实例，获取的workerId是否一样
    @Test
    public void testSameMarkAssigner(@Qualifier("uidGenerator2") UidGenerator uidGenerator1){
        long defaultUid=cachedUidGenerator.getUID();
        String defaultParse=cachedUidGenerator.parseUID(defaultUid);
        int defaultWorkerId=workerId(defaultParse);


        long uid1= uidGenerator1.getUID();
        String parse1=uidGenerator1.parseUID(uid1);
        int workerId1=workerId(parse1);


        Assert.assertNotEquals(defaultWorkerId, workerId1);

    }





    private int workerId(String parse){
        Pattern pattern=Pattern.compile(".*\"workerId\":\"(\\d+)\".*");

        Matcher matcher=pattern.matcher(parse);
        if(matcher.matches()){
            String workerIdStr=matcher.group(1);
            return Integer.valueOf(workerIdStr);
        }
        throw new RuntimeException("不匹配");
    }




}
