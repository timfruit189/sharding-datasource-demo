package com.ttx.transform.validate.common.validate;

import com.ttx.transform.validate.common.mapper.CommonMapper;
import lombok.Builder;
import lombok.Data;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;

/**
 * @author TimFruit
 * @date 20-4-22 上午11:28
 */
@Data
@Builder
public class ValidateConfig {

    private Logger log;

    //校验器区分标签
    private String tag;

    private Long startId;
    private Long endId;
    private Long timeSleep;
    //步长
    private Long stepLen;

    //最大检验次数
    private Integer maxValidateCount;

    //源库
    CommonMapper sourceMapper;
    Class<? extends CommonMapper> sourceMapperClass;
    String listEntitiesByRangeQueryId;
    SqlSessionFactory sourceSqlSessionFactory;//单库
    //源库标签
    private  String sourceTag;


    //目的库
    CommonMapper destMapper;
    private  String destTag;
}
