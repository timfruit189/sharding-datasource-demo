package com.ttx.transform.validate.common.config;

import com.ttx.transform.validate.common.mapper.OrderItemMapper;
import com.ttx.transform.validate.common.mapper.OrderMapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.shardingsphere.shardingjdbc.jdbc.core.datasource.ShardingDataSource;
import org.apache.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * sharding 配置
 * @author TimFruit
 * @date 20-4-11 下午5:01
 */
@Configuration
public class MybatisShardingConfig {

    /**
     * @see SpringBootConfiguration#shardingDataSource()
     */
    @Autowired
    @Qualifier("shardingDataSource")
    DataSource shardingDataSource;

    @Value("${mybatis.mapper-locations}")
    String mapperLocations;



    @Bean("shardingSqlSessionFactory")
    public SqlSessionFactory createSqlSessionFactory(@Qualifier("shardingDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean=new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        PathMatchingResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
        factoryBean.setMapperLocations(resolver.getResources(mapperLocations));
        SqlSessionFactory sqlSessionFactory = factoryBean.getObject();
        return sqlSessionFactory;
    }




    @Bean("shardingTransactionManager")
    public TransactionManager createTransactionManager(@Qualifier("shardingDataSource") DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean("shardingOrderMapper")
    public OrderMapper createOrderMapper(@Qualifier("shardingSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderMapper.class);
        return factoryBean.getObject();
    }


    @Bean("shardingOrderItemMapper")
    public OrderItemMapper createOrderItemMapper(@Qualifier("shardingSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        MapperFactoryBean<OrderItemMapper> factoryBean=new MapperFactoryBean<>();
        factoryBean.setSqlSessionFactory(sqlSessionFactory);
        factoryBean.setMapperInterface(OrderItemMapper.class);
        return factoryBean.getObject();
    }


//
//    //对应配置 默认库是单库ds-single ，需要排除
//    public static final String SINGLE_DATASOURCE_KEY="ds-single";
//
//    @Bean("shardingSqlSessionFactoryMap")
//    public Map<String,SqlSessionFactory> createShardingSqlSessionFactoryMap(@Qualifier("shardingDataSource") DataSource dataSource ) throws Exception {
//        ShardingDataSource shardingDataSource=(ShardingDataSource)dataSource;
//
//        Map<String,DataSource> dataSourceMap=shardingDataSource.getDataSourceMap();
//
//
//        Map<String, SqlSessionFactory> sessionFactoryMap=new HashMap<>();
//
//        String key;
//        DataSource source;
//        SqlSessionFactoryBean sessionFactoryBean;
//        SqlSessionFactory sessionFactory;
//
//        for(Map.Entry<String, DataSource> entry: dataSourceMap.entrySet()){
//
//            key=entry.getKey();
//            if(key.equals(SINGLE_DATASOURCE_KEY)){
//                continue;
//            }
//
//            source=entry.getValue();
//
//
//            sessionFactoryBean=new SqlSessionFactoryBean();
//            sessionFactoryBean.setDataSource(source);
//            PathMatchingResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
//            sessionFactoryBean.setMapperLocations(resolver.getResources(mapperLocations));
//
//            sessionFactory=sessionFactoryBean.getObject();
//            sessionFactoryMap.put(key, sessionFactory);
//        }
//
//
//        return sessionFactoryMap;
//    }
//
//
//    @Bean("shardingOrderMapperMap")
//    public Map<String, OrderMapper> createOrderMapperMap(@Qualifier("shardingSqlSessionFactoryMap") Map<String,SqlSessionFactory> sqlSessionFactoryMap) throws Exception {
//
//        Map<String, OrderMapper> orderMapperMap=new HashMap<>();
//
//
//        String key=null;
//        SqlSessionFactory sqlSessionFactory;
//        for(Map.Entry<String, SqlSessionFactory> entry: sqlSessionFactoryMap.entrySet()){
//
//            key=entry.getKey();
//            sqlSessionFactory=entry.getValue();
//
//            MapperFactoryBean<OrderMapper> factoryBean=new MapperFactoryBean<>();
//            factoryBean.setSqlSessionFactory(sqlSessionFactory);
//            factoryBean.setMapperInterface(OrderMapper.class);
//
//            orderMapperMap.put(key, factoryBean.getObject());
//        }
//
//
//        return orderMapperMap;
//
//    }
//
//
//    @Bean("shardingOrderItemMapperMap")
//    public Map<String, OrderItemMapper> createOrderItemMapperMap(@Qualifier("shardingSqlSessionFactoryMap") Map<String,SqlSessionFactory> sqlSessionFactoryMap) throws Exception {
//        Map<String, OrderItemMapper> itemMapperMap=new HashMap<>();
//
//        String key=null;
//        SqlSessionFactory sqlSessionFactory;
//        for(Map.Entry<String, SqlSessionFactory> entry: sqlSessionFactoryMap.entrySet()){
//
//            key=entry.getKey();
//            sqlSessionFactory=entry.getValue();
//
//            MapperFactoryBean<OrderItemMapper> factoryBean=new MapperFactoryBean<>();
//            factoryBean.setSqlSessionFactory(sqlSessionFactory);
//            factoryBean.setMapperInterface(OrderItemMapper.class);
//
//            itemMapperMap.put(key, factoryBean.getObject());
//        }
//
//        return itemMapperMap;
//
//    }


}
