package com.ttx.transform.validate.common.util;


import com.ttx.transform.validate.common.constant.StepType;

/**
 * @author TimFruit
 * @date 20-4-14 下午5:25
 */
public class SpringBootUtil {
    public static void setServerPort( int port){
        System.setProperty("server.port",String.valueOf(port));
    }

    public static void setStep(String step){
        System.setProperty(StepType.STEP, step);
    }
}
