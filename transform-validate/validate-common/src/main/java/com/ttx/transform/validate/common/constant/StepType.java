package com.ttx.transform.validate.common.constant;

/**
 * @author TimFruit
 * @date 20-4-14 下午6:01
 */
public interface StepType {
    String STEP="STEP";
    String step1="1";
    String step2="2";
    String step3="3";
}
