package com.ttx.transform.validate.common.model.dataobject;

import java.util.Date;

/**
 * @author TimFruit
 * @date 20-4-15 上午9:51
 */
public abstract class ShardingKeyDO {

    /**
     * 获得主键
     * @return
     */
    public abstract Long getDoId();

    /**
     * 分片
     * @return
     */
    public abstract int getUserId();


    public abstract Date getUpdateTime();


}
