package com.ttx.transform.validate.common.mapper;

import com.ttx.transform.validate.common.model.dataobject.OrderItemDO;
import com.ttx.transform.validate.common.model.dataobject.ShardingKeyDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TimFruit
 * @date 20-4-15 上午9:36
 */
public interface CommonMapper<D> {
    /**
     * 自定义id
     * @param entity
     * @return
     */
    Long insertWidthId(D entity);

    //包括startId,endId
    Long countEntities(@Param("startId") Long startId, @Param("endId") Long endId);


    //不包括beginId,包括endId
    List<D> listEntitiesByRange(@Param("beginId")Long beginId, @Param("endId")Long endId);

    D selectEntityById(@Param("id") Long id);

    D selectEntityByIdAndUserId(@Param("id") Long id, @Param("userId") Integer userId);

    //包括startId
    Long selectMaxIdInRange(@Param("startId")Long startId, @Param("endId")Long endId);

    //包括startId
    Long selectMinIdInRange(@Param("startId")Long startId, @Param("endId")Long endId);

    void updateByIdAndUserId(D entity);
}
