package com.ttx.transform.validate.common.util;

import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;

/**
 * @author TimFruit
 * @date 20-4-22 下午3:16
 */
public class JsonUtil {
    private static JSONConfig config=new JSONConfig();
    static {
        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
    }


    public static String format(Object obj){
        return JSONUtil.wrap(obj,config).toString();
    }
}
