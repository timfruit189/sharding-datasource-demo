package com.ttx.transform.validate.common.validate;

/**
 * @author TimFruit
 * @date 20-4-19 下午12:00
 */
public interface IValidator {
    void validate();
}
