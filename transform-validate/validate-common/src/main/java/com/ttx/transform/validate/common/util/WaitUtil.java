package com.ttx.transform.validate.common.util;

import java.util.concurrent.CountDownLatch;

/**
 * @author TimFruit
 * @date 20-4-14 下午4:16
 */
public class WaitUtil {
    private static CountDownLatch cdl=new CountDownLatch(1);


    public static void await(){
        try {
            cdl.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void release(){
        cdl.countDown();
    }

}
