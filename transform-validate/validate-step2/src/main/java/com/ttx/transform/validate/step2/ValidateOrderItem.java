package com.ttx.transform.validate.step2;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.ttx.transform.validate.common.mapper.OrderItemMapper;
import com.ttx.transform.validate.common.mapper.OrderMapper;
import com.ttx.transform.validate.common.model.dataobject.OrderItemDO;
import com.ttx.transform.validate.common.validate.CommonDBValidator;
import com.ttx.transform.validate.common.validate.IValidator;
import com.ttx.transform.validate.common.validate.ValidateConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 配置检验orderItem 双写 单库 -> 分库)
 * @author TimFruit
 * @date 20-4-14 下午6:28
 */
@Component
@Scope("prototype")
@Slf4j
public class ValidateOrderItem implements IValidator,DisposableBean {
    @Autowired
    OrderItemMapper singleOrderItemMapper;
    @Autowired
    OrderItemMapper shardingOrderItemMapper;

    @Autowired
    SqlSessionFactory defaultSqlSessionFactory;

//    @Value("${validate.start-order-item-id}")
    Long startOrderItemId;
//    @Value("${validate.end-order-item-id}")
    Long endOrderItemId;

//    @Value("${validate.timesleep.seconds}")
    Long timeSleep;

    //步长
//    @Value("${validate.step.len}")
    Long stepLen;

    //最大检验次数
    @Value("${validate.maxValidateCount}")
    private Integer maxValidateCount;

    CommonDBValidator validate;
    @Override
    public void validate() {
        ValidateConfig dto=ValidateConfig.builder()
                .log(log)

                .tag("orderItem")


                .sourceTag("单库")
                .sourceSqlSessionFactory(defaultSqlSessionFactory)
                .sourceMapper(singleOrderItemMapper)
                .sourceMapperClass(OrderItemMapper.class)

                .destTag("分库")
                .destMapper(shardingOrderItemMapper)


                .maxValidateCount(maxValidateCount)
                .stepLen(stepLen)
                .startId(startOrderItemId)
                .endId(endOrderItemId)
                .timeSleep(timeSleep)
                .build();


        validate=new CommonDBValidator(dto);

        validate.validate();
    }

    @Override
    public void destroy() throws Exception {
        if(validate!=null){
            validate.shutdown();
        }
    }
}
