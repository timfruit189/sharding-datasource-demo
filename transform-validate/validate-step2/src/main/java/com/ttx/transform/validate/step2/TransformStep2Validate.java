package com.ttx.transform.validate.step2;

import com.ttx.transform.validate.common.constant.StepType;
import com.ttx.transform.validate.common.util.SpringBootUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author TimFruit
 * @date 20-4-14 上午11:10
 */
@SpringBootApplication(scanBasePackages = "com.ttx")
public class TransformStep2Validate {



    public static void main(String[] args) throws InterruptedException {

        SpringApplication.run(TransformStep2Validate.class, args);

    }







}
