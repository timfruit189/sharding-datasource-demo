package com.ttx.transform.validate.step3;

import com.ttx.transform.validate.common.mapper.OrderMapper;
import com.ttx.transform.validate.common.validate.CommonDBValidator;
import com.ttx.transform.validate.common.validate.IValidator;
import com.ttx.transform.validate.common.validate.ValidateConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 配置检验order（分库 -> 单库)
 * @author TimFruit
 * @date 20-4-14 下午6:28
 */
@Component
@Scope("prototype")
@Slf4j

public class ValidateOrder implements IValidator,DisposableBean {

    @Autowired
    OrderMapper singleOrderMapper;


    @Autowired
    OrderMapper shardingOrderMapper;

    @Autowired
    SqlSessionFactory shardingSqlSessionFactory;

//    @Value("${validate.start-order-id}")
    Long startOrderId;
//    @Value("${validate.end-order-id}")
    Long endOrderId;



    //步长
//    @Value("${validate.step.len}")
    Long stepLen;

    //最大检验次数
    @Value("${validate.maxValidateCount}")
    private Integer maxValidateCount;

//    @Value("${validate.timesleep.seconds}")
    Long timeSleep;

    CommonDBValidator validate;

    @Override
    public void validate() {
        ValidateConfig dto=ValidateConfig.builder()
                .log(log)

                .tag("order")


                .sourceTag("分库")
                .sourceSqlSessionFactory(shardingSqlSessionFactory)
                .sourceMapper(shardingOrderMapper)
                .sourceMapperClass(OrderMapper.class)

                .destTag("单库")
                .destMapper(singleOrderMapper)


                .maxValidateCount(maxValidateCount)
                .stepLen(stepLen)
                .startId(startOrderId)
                .endId(endOrderId)
                .timeSleep(timeSleep)
                .build();


        validate=new CommonDBValidator(dto);

        validate.validate();
    }

    @Override
    public void destroy() throws Exception {
        if(validate!=null){
            validate.shutdown();
        }
    }
}
