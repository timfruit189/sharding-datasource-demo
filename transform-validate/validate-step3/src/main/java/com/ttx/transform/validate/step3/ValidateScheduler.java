package com.ttx.transform.validate.step3;

import cn.hutool.core.thread.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author TimFruit
 * @date 20-4-19 上午11:58
 */
@Component

@Slf4j
public class ValidateScheduler implements CommandLineRunner {


    //对应于1
    @Value("${validate.step.len-1}")
    private Long stepLen_1;

    //对应于2
    @Value("${validate.step.len-2}")
    private Long stepLen_2;

        @Value("${validate.start-order-id-1}")
    Long startOrderId_1;
        @Value("${validate.end-order-id-1}")
    Long endOrderId_1;

    @Value("${validate.start-order-id-2}")
    Long startOrderId_2;
    @Value("${validate.end-order-id-2}")
    Long endOrderId_2;

        @Value("${validate.timesleep.seconds-1}")
    Long timeSleep_1;

    @Value("${validate.timesleep.seconds-2}")
    Long timeSleep_2;

    @Autowired
    ValidateOrder validateOrder1;

    @Autowired
    ValidateOrder validateOrder2;




        @Value("${validate.start-order-item-id-1}")
    Long startOrderItemId_1;
        @Value("${validate.end-order-item-id-1}")
    Long endOrderItemId_1;


    @Value("${validate.start-order-item-id-2}")
    Long startOrderItemId_2;
    @Value("${validate.end-order-item-id-2}")
    Long endOrderItemId_2;


    @Autowired
    ValidateOrderItem validateOrderItem1;

    @Autowired
    ValidateOrderItem validateOrderItem2;

    @Override
    public void run(String... args) throws Exception {
        int threadCount=4;
        ExecutorService executorService=Executors.newFixedThreadPool(threadCount,
                new NamedThreadFactory("validateThread",false));
        executorService.execute(()->{
            validateOrder1.startOrderId=startOrderId_1;
            validateOrder1.endOrderId=endOrderId_1;
            validateOrder1.stepLen=stepLen_1;
            validateOrder1.timeSleep=timeSleep_1;
            validateOrder1.validate();
        });
        executorService.execute(()->{
            validateOrderItem1.startOrderItemId=startOrderItemId_1;
            validateOrderItem1.endOrderItemId=endOrderItemId_1;
            validateOrderItem1.stepLen=stepLen_1;
            validateOrderItem1.timeSleep=timeSleep_1;
            validateOrderItem1.validate();
        });
        executorService.execute(()->{
            validateOrder2.startOrderId=startOrderId_2;
            validateOrder2.endOrderId=endOrderId_2;
            validateOrder2.stepLen=stepLen_2;
            validateOrder2.timeSleep=timeSleep_2;
            validateOrder2.validate();
        });
        executorService.execute(()->{
            validateOrderItem2.startOrderItemId=startOrderItemId_2;
            validateOrderItem2.endOrderItemId=endOrderItemId_2;
            validateOrderItem2.stepLen=stepLen_2;
            validateOrderItem2.timeSleep=timeSleep_2;
            validateOrderItem2.validate();
        });
    }

}
